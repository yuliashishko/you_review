from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.db import get_session
from app.models.dto import SkillCreate, SkillUpdate
from app.repos.repositories import get_skill_repo
from app.repos.skill_repo import SkillRepo

router = APIRouter()


@router.get("/")
def get_skills(skill_repo: SkillRepo = Depends(get_skill_repo),
               session: Session = Depends(get_session)):
    with session.begin():
        return skill_repo.get_skills()


@router.get("/{id}")
def get_skill(id: int,
              skill_repo: SkillRepo = Depends(get_skill_repo),
              session: Session = Depends(get_session)):
    with session.begin():
        return skill_repo.get_skill(id)


@router.post("/")
def create_skill(req: SkillCreate,
                 skill_repo: SkillRepo = Depends(get_skill_repo),
                 session: Session = Depends(get_session)):
    with session.begin():
        id = skill_repo.create_skill(req)
        return skill_repo.get_skill(id)


@router.put("/")
def update_skill(req: SkillUpdate,
                 skill_repo: SkillRepo = Depends(get_skill_repo),
                 session: Session = Depends(get_session)):
    with session.begin():
        id = skill_repo.update_skill(req)
        return skill_repo.get_skill(id)

from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session

from app.db import get_session
from app.models.dto import SeekerDTO, SeekerCreate, SeekerUpdate, VacancyDTO
from app.repos.repositories import get_seeker_repo
from app.repos.seeker_repo import SeekerRepo
from app.service.matching_serivce import MatchingService
from app.service.rating_service import RatingService
from app.service.services import get_matching_service, get_rating_service

router = APIRouter()


@router.get("/", response_model=list[SeekerDTO])
def get_seekers(seeker_repo: SeekerRepo = Depends(get_seeker_repo),
                session: Session = Depends(get_session)):
    with session.begin():
        return seeker_repo.get_seekers()


@router.get("/{id}", response_model=SeekerDTO)
def get_seeker(id: int,
               seeker_repo: SeekerRepo = Depends(get_seeker_repo),
               session: Session = Depends(get_session)):
    with session.begin():
        return seeker_repo.get_seeker(id)


@router.get("/{id}/vacancies", response_model=list[VacancyDTO])
def get_seeker_vacancy_suggestions(id: int,
                                   matching_service: MatchingService = Depends(get_matching_service),
                                   session: Session = Depends(get_session)):
    with session.begin():
        return matching_service.get_vacancies(id)


@router.get('/{id}/rating', response_model=float)
def get_seeker_rating(id: int,
                      session: Session = Depends(get_session),
                      rating_service: RatingService = Depends(get_rating_service)):
    with session.begin():
        return rating_service.get_seeker_rating(id)


@router.post("/")
def create_seeker(req: SeekerCreate,
                  seeker_repo: SeekerRepo = Depends(get_seeker_repo),
                  session: Session = Depends(get_session)):
    with session.begin():
        id = seeker_repo.create_seeker(req)
        return seeker_repo.get_seeker(id)


@router.put("/")
def update_seeker(req: SeekerUpdate,
                  seeker_repo: SeekerRepo = Depends(get_seeker_repo),
                  session: Session = Depends(get_session)):
    with session.begin():
        id = seeker_repo.update_seeker(req)
        return seeker_repo.get_seeker(id)


@router.get("/by_company/{company_id}", response_model=list[SeekerDTO])
def get_seekers(company_id: int,
                seeker_repo: SeekerRepo = Depends(get_seeker_repo),
                session: Session = Depends(get_session)):
    with session.begin():
        return seeker_repo.get_seeker_by_company(company_id)

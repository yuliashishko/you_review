from fastapi import APIRouter

from app.routers import seekers, companies, skills, vacancies, work_formats, interviews, feeedbacks

api_router = APIRouter()
api_router.include_router(seekers.router, prefix="/seekers", tags=['Seekers'])
api_router.include_router(companies.router, prefix="/companies", tags=['Companies'])
api_router.include_router(skills.router, prefix="/skills", tags=['Skills'])
api_router.include_router(vacancies.router, prefix="/vacancies", tags=['Vacancies'])
api_router.include_router(work_formats.router, prefix="/work-formats", tags=['WorkFormats'])
api_router.include_router(interviews.router_interview, prefix="/interviews", tags=['Interviews'])
api_router.include_router(interviews.router_status, prefix="/interview-statuses", tags=['Interview Statuses'])
api_router.include_router(feeedbacks.router, prefix="/feedbacks", tags=['Feedbacks'])

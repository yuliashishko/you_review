from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.db import get_session
from app.models.dto import InterviewDTO, InterviewCreate, InterviewStatusDTO, InterviewStatusCreate, InterviewUpdate
from app.repos.interview_repo import InterviewRepo
from app.repos.repositories import get_interview_repo

router_interview = APIRouter()


@router_interview.get('/', response_model=list[InterviewDTO])
def get_interviews(session: Session = Depends(get_session),
                   interview_repo: InterviewRepo = Depends(get_interview_repo)):
    with session.begin():
        return interview_repo.get_interviews()


@router_interview.get('/{id}', response_model=InterviewDTO)
def get_interview(id: int,
                  session: Session = Depends(get_session),
                  interview_repo: InterviewRepo = Depends(get_interview_repo)):
    with session.begin():
        return interview_repo.get_interview(id)


@router_interview.post('/', response_model=InterviewDTO)
def create_interview(req: InterviewCreate,
                     session: Session = Depends(get_session),
                     interview_repo: InterviewRepo = Depends(get_interview_repo)):
    with session.begin():
        id = interview_repo.create_interview(req)
        return interview_repo.get_interview(id)


@router_interview.put('/', response_model=InterviewDTO)
def create_interview(req: InterviewUpdate,
                     session: Session = Depends(get_session),
                     interview_repo: InterviewRepo = Depends(get_interview_repo)):
    with session.begin():
        id = interview_repo.update_interview(req)
        return interview_repo.get_interview(id)


router_status = APIRouter()


@router_status.get('/', response_model=list[InterviewStatusDTO])
def get_interview_statuses(session: Session = Depends(get_session),
                           interview_repo: InterviewRepo = Depends(get_interview_repo)):
    with session.begin():
        return interview_repo.get_interview_statuses()


@router_status.get('/{id}', response_model=InterviewStatusDTO)
def get_interview_status(id: int,
                         session: Session = Depends(get_session),
                         interview_repo: InterviewRepo = Depends(get_interview_repo)):
    with session.begin():
        return interview_repo.get_interview_status(id)


@router_status.post('/')
def create_interview_status(req: InterviewStatusCreate,
                            session: Session = Depends(get_session),
                            interview_repo: InterviewRepo = Depends(get_interview_repo)):
    with session.begin():
        id = interview_repo.create_interview_status(req)
        return interview_repo.get_interview_status(id)

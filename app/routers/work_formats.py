from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.db import get_session
from app.models.dto import WorkFormatCreate, WorkFormatUpdate
from app.repos.repositories import get_work_format_repo
from app.repos.work_format_repo import WorkFormatRepo

router = APIRouter()


@router.get("/")
def get_work_formats(work_format_repo: WorkFormatRepo = Depends(get_work_format_repo),
                     session: Session = Depends(get_session)):
    with session.begin():
        return work_format_repo.get_work_formats()


@router.get("/{id}")
def get_work_format(id: int,
                    work_format_repo: WorkFormatRepo = Depends(get_work_format_repo),
                    session: Session = Depends(get_session)):
    with session.begin():
        return work_format_repo.get_work_format(id)


@router.post("/")
def create_work_format(req: WorkFormatCreate,
                       work_format_repo: WorkFormatRepo = Depends(get_work_format_repo),
                       session: Session = Depends(get_session)):
    with session.begin():
        id = work_format_repo.create_work_format(req)
        return work_format_repo.get_work_format(id)


@router.put("/")
def update_work_format(req: WorkFormatUpdate,
                       work_format_repo: WorkFormatRepo = Depends(get_work_format_repo),
                       session: Session = Depends(get_session)):
    with session.begin():
        id = work_format_repo.update_work_format(req)
        return work_format_repo.get_work_format(id)

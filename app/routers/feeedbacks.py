from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.db import get_session
from app.models.dto import FeedbackDTO, FeedbackCreate, FeedbackUpdate
from app.repos.feedback_repo import FeedbackRepo
from app.repos.repositories import get_feedback_repo

router = APIRouter()


@router.get('/{interview_id}', response_model=list[FeedbackDTO])
def get_feedbacks(interview_id: int,
                  session: Session = Depends(get_session),
                  feedback_repo: FeedbackRepo = Depends(get_feedback_repo)):
    with session.begin():
        return feedback_repo.get_feedbacks(interview_id)


@router.post('/{interview_id}/seeker')
def create_seeker_feedback(interview_id: int,
                           req: FeedbackCreate,
                           session: Session = Depends(get_session),
                           feedback_repo: FeedbackRepo = Depends(get_feedback_repo)):
    with session.begin():
        id = feedback_repo.create_seeker_feedback(interview_id, req)
        return feedback_repo.get_feedback(id)


@router.post('/{interview_id}/company')
def create_company_feedback(interview_id: int,
                            req: FeedbackCreate,
                            session: Session = Depends(get_session),
                            feedback_repo: FeedbackRepo = Depends(get_feedback_repo)):
    with session.begin():
        id = feedback_repo.create_company_feedback(interview_id, req)
        return feedback_repo.get_feedback(id)


@router.put('/')
def update_feedback(req: FeedbackUpdate,
                    session: Session = Depends(get_session),
                    feedback_repo: FeedbackRepo = Depends(get_feedback_repo)):
    with session.begin():
        id = feedback_repo.update_feedback(req)
        return feedback_repo.get_feedback(id)

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app.db import get_session
from app.models.dto import CompanyCreate, CompanyDTO, CompanyUpdate
from app.repos.company_repo import CompanyRepo
from app.repos.repositories import get_company_repo
from app.service.rating_service import RatingService
from app.service.services import get_rating_service

router = APIRouter()


@router.get("/")
def get_companies(company_repo: CompanyRepo = Depends(get_company_repo),
                  session: Session = Depends(get_session)):
    with session.begin():
        return company_repo.get_companies()


@router.get("/{id}", response_model=CompanyDTO)
def get_company(id: int,
                company_repo: CompanyRepo = Depends(get_company_repo),
                session: Session = Depends(get_session)):
    with session.begin():
        return company_repo.get_company(id)


@router.get('/{id}/rating', response_model=float)
def get_company_rating(id: int,
                       session: Session = Depends(get_session),
                       rating_service: RatingService = Depends(get_rating_service)):
    with session.begin():
        return rating_service.get_company_rating(id)


@router.post("/")
def create_company(req: CompanyCreate,
                   company_repo: CompanyRepo = Depends(get_company_repo),
                   session: Session = Depends(get_session)):
    with session.begin():
        id = company_repo.create_company(req)
        return company_repo.get_company(id)


@router.put("/")
def update_company(req: CompanyUpdate,
                   company_repo: CompanyRepo = Depends(get_company_repo),
                   session: Session = Depends(get_session)):
    with session.begin():
        id = company_repo.update_company(req)
        return company_repo.update_company(req)

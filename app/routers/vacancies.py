from fastapi import Depends, APIRouter
from sqlalchemy.orm import Session

from app.db import get_session
from app.models.dto import VacancyDTO, VacancyCreate, VacancyUpdate, CompanyDTO, SeekerDTO
from app.repos.company_repo import CompanyRepo
from app.repos.matching_repo import MatchingRepo
from app.repos.repositories import get_vacancy_repo, get_company_repo, get_matching_repo
from app.repos.vacancy_repo import VacancyRepo
from app.service.matching_serivce import MatchingService
from app.service.services import get_matching_service

router = APIRouter()


@router.get("/", response_model=list[VacancyDTO])
def get_vacancies(vacancy_repo: VacancyRepo = Depends(get_vacancy_repo),
                  session: Session = Depends(get_session)):
    with session.begin():
        return vacancy_repo.get_vacancies()


@router.get("/{id}", response_model=VacancyDTO)
def get_vacancy(id: int,
                vacancy_repo: VacancyRepo = Depends(get_vacancy_repo),
                session: Session = Depends(get_session)):
    with session.begin():
        return vacancy_repo.get_vacancy(id)


@router.get("/{id}/seekers", response_model=list[SeekerDTO])
def get_seeker_vacancy_suggestions(id: int,
                                   matching_service: MatchingService = Depends(get_matching_service),
                                   session: Session = Depends(get_session)):
    with session.begin():
        return matching_service.get_seekers(id)


@router.post("/")
def create_vacancy(req: VacancyCreate,
                   vacancy_repo: VacancyRepo = Depends(get_vacancy_repo),
                   session: Session = Depends(get_session)):
    with session.begin():
        id = vacancy_repo.create_vacancy(req)
        return vacancy_repo.get_vacancy(id)


@router.put("/")
def update_vacancy(req: VacancyUpdate,
                   vacancy_repo: VacancyRepo = Depends(get_vacancy_repo),
                   session: Session = Depends(get_session)):
    with session.begin():
        id = vacancy_repo.update_vacancy(req)
        return vacancy_repo.get_vacancy(id)

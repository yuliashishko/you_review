import os

from fastapi import Depends, Request
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

DATABASE_URL = os.environ.get("DATABASE_URL")

engine = create_engine(DATABASE_URL, echo=True, future=True)


def get_session(request: Request) -> Session:
    session: Session = request.app.state.session_factory()

    try:
        yield session
    finally:
        session.commit()
        session.close()


# def get_session() -> Session:
#     with Session(engine) as session:
#         yield session

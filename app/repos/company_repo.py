from sqlalchemy import insert
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from app.models import parse_pydantic_schema
from app.models.orm import Company, User
from app.models.dto import CompanyDTO, CompanyCreate, CompanyUpdate


class CompanyRepo:
    def __init__(self, session: Session):
        self._session = session

    def get_companies(self):
        aw = self._session.execute(select(Company))
        result = aw.unique().all()
        return [CompanyDTO.from_orm(r[0]) for r in result]

    def get_company(self, id: int):
        aw = self._session.execute(select(Company).where(Company.id == id))
        result = aw.unique().one()
        return CompanyDTO.from_orm(result[0])

    def create_company(self, req: CompanyCreate):
        parsed_schema = parse_pydantic_schema(req)
        company = Company(**parsed_schema)
        self._session.add(company.user)
        self._session.flush([company.user])
        self._session.add(company)
        self._session.flush([company])
        return company.id

    def update_company(self, req: CompanyUpdate):
        parsed_schema = parse_pydantic_schema(req)
        company = Company(**parsed_schema)
        self._session.merge(company.user)
        self._session.merge(company)
        return company.id

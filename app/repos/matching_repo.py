from sqlalchemy import select
from sqlalchemy.orm import Session

from app.models.dto import VacancyDTO, SeekerDTO
from app.models.orm import Vacancy, vacancy_skill, Seeker, seeker_skill
from app.repos.utils import map_orms


class MatchingRepo:
    def __init__(self, session: Session):
        self._session = session

    def get_vacancies_contains_skills(self, skill_ids: list[int]):
        stmt = select(Vacancy).join(vacancy_skill).where(vacancy_skill.c.skill_id.in_(skill_ids))
        result = self._session.execute(stmt).unique().all()
        return map_orms(VacancyDTO, result)

    def get_seekers_contains_skills(self, skill_ids: list[int]):
        stmt = select(Seeker).join(seeker_skill).where(seeker_skill.c.skill_id.in_(skill_ids))
        result = self._session.execute(stmt).unique().all()
        return map_orms(SeekerDTO, result)

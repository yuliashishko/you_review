from sqlalchemy import insert
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from app.models import parse_pydantic_schema
from app.models.dto import WorkFormatDTO, WorkFormatCreate, WorkFormatUpdate
from app.models.orm import WorkFormat


class WorkFormatRepo:
    def __init__(self, session: Session):
        self._session = session

    def get_work_formats(self):
        aw = self._session.execute(select(WorkFormat))
        result = aw.all()
        return [WorkFormatDTO.from_orm(r[0]) for r in result]

    def get_work_format(self, id: int):
        aw = self._session.execute(select(WorkFormat).where(WorkFormat.id == id))
        result = aw.one()
        return WorkFormatDTO.from_orm(result[0])

    def create_work_format(self, req: WorkFormatCreate):
        statement = insert(WorkFormat).values(**req.dict())
        result = self._session.execute(statement)
        return result.inserted_primary_key[0]

    def update_work_format(self, req: WorkFormatUpdate):
        parsed_schema = parse_pydantic_schema(req)
        skill = WorkFormat(**parsed_schema)
        self.get_work_format(req.id)
        self._session.merge(skill)
        return skill.id
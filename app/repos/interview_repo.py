from sqlalchemy import select, insert
from sqlalchemy.orm import Session

from app.models.dto import InterviewDTO, InterviewCreate, InterviewStatusDTO, InterviewStatusCreate, InterviewUpdate
from app.models.orm import Interview, InterviewStatus, Vacancy
from app.repos.utils import map_orms, map_orm, get_primary_key_from_create


class InterviewRepo:
    def __init__(self, session: Session):
        self._session = session

    def get_interviews(self):
        result = self._session.execute(select(Interview)).all()
        return map_orms(InterviewDTO, result)

    def get_interview(self, id: int):
        result = self._session.execute(select(Interview).where(Interview.id == id)).one()
        return map_orm(InterviewDTO, result)

    def create_interview(self, req: InterviewCreate):
        res = self._session.execute(insert(Interview).values(**req.dict()))
        return get_primary_key_from_create(res)

    def update_interview(self, req: InterviewUpdate):
        interview: Interview = self._session.get(Interview, req.id)
        interview.status_id = req.status_id
        self._session.merge(interview)
        return interview.id

    def get_interview_statuses(self):
        result = self._session.execute(select(InterviewStatus)).all()
        return map_orms(InterviewStatusDTO, result)

    def get_interview_status(self, id: int):
        result = self._session.execute(select(InterviewStatus).where(InterviewStatus.id == id)).one()
        return map_orm(InterviewStatusDTO, result)

    def create_interview_status(self, req: InterviewStatusCreate):
        res = self._session.execute(insert(InterviewStatus).values(**req.dict()))
        return get_primary_key_from_create(res)

    def get_seeker_interview(self, seeker_id: int):
        results = self._session.execute(select(Interview).where(Interview.seeker_id == seeker_id)).unique().all()
        return map_orms(InterviewDTO, results)

    def get_company_interview(self, company_id: int):
        results = self._session.execute(
            select(Interview).join(Vacancy).where(Vacancy.company_id == company_id)).unique().all()
        return map_orms(InterviewDTO, results)

from sqlalchemy import insert
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from app.models import parse_pydantic_schema
from app.models.dto import SkillDTO, SkillCreate, SkillUpdate
from app.models.orm import Skill


class SkillRepo:
    def __init__(self, session: Session):
        self._session = session

    def get_skills(self):
        aw = self._session.execute(select(Skill))
        result = aw.all()
        return [SkillDTO.from_orm(r[0]) for r in result]

    def get_skill(self, id: int):
        aw = self._session.execute(select(Skill).where(Skill.id == id))
        result = aw.one()
        return SkillDTO.from_orm(result[0])

    def create_skill(self, req: SkillCreate):
        statement = insert(Skill).values(**req.dict())
        result = self._session.execute(statement)
        return result.inserted_primary_key[0]

    def update_skill(self, req: SkillUpdate):
        parsed_schema = parse_pydantic_schema(req)
        skill = Skill(**parsed_schema)
        self.get_skill(req.id)
        self._session.merge(req)
        return skill.id

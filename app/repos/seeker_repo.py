from sqlalchemy.future import select
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from app.models import parse_pydantic_schema
from app.models.dto import SeekerDTO, SeekerCreate, SeekerUpdate
from app.models.orm import Seeker, Skill, Interview, Company, InterviewStatus
from app.repos.utils import map_orms


class SeekerRepo:
    def __init__(self, session: Session):
        self._session = session

    def get_seekers(self):
        aw = self._session.execute(select(Seeker))
        result = aw.unique().all()
        # The unique() method must be invoked on this Result, as it contains results that include joined eager loads against collections
        seekers = [SeekerDTO.from_orm(r[0]) for r in result]
        return seekers

    def get_seeker(self, id: int):
        aw = self._session.execute(select(Seeker).where(Seeker.id == id))
        # The unique() method must be invoked on this Result, as it contains results that include joined eager loads against collections
        result = aw.unique().one()
        return SeekerDTO.from_orm(result[0])

    def create_seeker(self, req: SeekerCreate):
        parsed_schema = parse_pydantic_schema(req)
        seeker = Seeker(**parsed_schema)
        self._session.add(seeker.user)
        self._session.flush([seeker.user])
        seeker.skills = [Skill.as_unique(self._session, id=s) for s in req.skills]
        self._session.add(seeker)
        self._session.flush([seeker])
        return seeker.id

    def update_seeker(self, req: SeekerUpdate):
        parsed_schema = parse_pydantic_schema(req)
        update_seeker = Seeker(**parsed_schema)
        update_seeker.skills = [Skill.as_unique(self._session, id=s) for s in req.skills]
        self.get_seeker(req.id)
        self._session.merge(update_seeker.user)
        self._session.merge(update_seeker)
        return update_seeker.id

    def get_seeker_by_company(self, company_id: int):
        query = self._session.query(Seeker, Interview, Company, InterviewStatus)
        query = query.filter(Company.id == company_id).filter(
            InterviewStatus.name.in_(["Успешно", "Успешно принят на работу"])).all()
        return map_orms(SeekerDTO, query)

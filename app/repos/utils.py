from typing import Type

from app.models import BaseDTO


def map_orm(dto_class: Type[BaseDTO], row):
    return dto_class.from_orm(row[0])


def map_orms(dto_class: Type[BaseDTO], rows):
    return [dto_class.from_orm(row[0]) for row in rows]


def get_primary_key_from_create(result):
    return result.inserted_primary_key[0]

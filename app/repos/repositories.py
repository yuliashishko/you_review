from fastapi import Depends
from sqlalchemy.orm import Session

from app.db import get_session
from app.repos.company_repo import CompanyRepo
from app.repos.feedback_repo import FeedbackRepo
from app.repos.interview_repo import InterviewRepo
from app.repos.matching_repo import MatchingRepo
from app.repos.seeker_repo import SeekerRepo
from app.repos.skill_repo import SkillRepo
from app.repos.vacancy_repo import VacancyRepo
from app.repos.work_format_repo import WorkFormatRepo


def get_company_repo(session: Session = Depends(get_session)) -> CompanyRepo:
    return CompanyRepo(session)


def get_seeker_repo(session: Session = Depends(get_session)) -> SeekerRepo:
    return SeekerRepo(session)


def get_skill_repo(session: Session = Depends(get_session)) -> SkillRepo:
    return SkillRepo(session)


def get_vacancy_repo(session: Session = Depends(get_session)) -> VacancyRepo:
    return VacancyRepo(session)


def get_work_format_repo(session: Session = Depends(get_session)) -> WorkFormatRepo:
    return WorkFormatRepo(session)


def get_interview_repo(session: Session = Depends(get_session)) -> InterviewRepo:
    return InterviewRepo(session)


def get_feedback_repo(session: Session = Depends(get_session)) -> FeedbackRepo:
    return FeedbackRepo(session)


def get_matching_repo(session: Session = Depends(get_session)) -> MatchingRepo:
    return MatchingRepo(session)

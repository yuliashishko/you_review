from sqlalchemy import insert
from sqlalchemy.future import select
from sqlalchemy.orm import Session

from app.models import parse_pydantic_schema
from app.models.dto import VacancyDTO, VacancyCreate, VacancyUpdate
from app.models.orm import Vacancy, Skill


class VacancyRepo:
    def __init__(self, session: Session):
        self._session = session

    def get_vacancies(self):
        aw = self._session.execute(select(Vacancy))
        result = aw.unique().all()
        # The unique() method must be invoked on this Result, as it contains results that include joined eager loads against collections
        vacancies = [VacancyDTO.from_orm(r[0]) for r in result]
        return vacancies

    def get_vacancy(self, id: int):
        aw = self._session.execute(select(Vacancy).where(Vacancy.id == id))
        # The unique() method must be invoked on this Result, as it contains results that include joined eager loads against collections
        result = aw.unique().one()
        return VacancyDTO.from_orm(result[0])

    def _get_skills(self, ids: list[int]):
        aw = self._session.execute(select(Skill).where(Skill.id.in_(ids)))
        return [r[0] for r in aw.all()]

    def create_vacancy(self, req: VacancyCreate):
        parsed_schema = parse_pydantic_schema(req)
        vacancy = Vacancy(**parsed_schema)
        vacancy.skills = [Skill.as_unique(self._session, id=s) for s in req.skills]
        self._session.add(vacancy)
        self._session.flush(objects=[vacancy])
        return vacancy.id

    def update_vacancy(self, req: VacancyUpdate):
        parsed_schema = parse_pydantic_schema(req)
        vacancy = Vacancy(**parsed_schema)
        vacancy.skills = [Skill.as_unique(self._session, id=s) for s in req.skills]
        self.get_vacancy(req.id)
        self._session.merge(vacancy)
        return vacancy.id

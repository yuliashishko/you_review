from sqlalchemy import select, insert
from sqlalchemy.orm import Session

from app.models.dto import FeedbackDTO, FeedbackCreate, FeedbackUpdate
from app.models.orm import Feedback, Interview
from app.repos.utils import map_orms, map_orm, get_primary_key_from_create


class FeedbackRepo:
    def __init__(self, session: Session):
        self._session = session

    def get_feedbacks(self, id):
        result = self._session.execute(select(Feedback).where(Feedback.interview_id == id)).all()
        return map_orms(FeedbackDTO, result)

    def get_feedback(self, id: int):
        result = self._session.execute(select(Feedback).where(Feedback.id == id)).one()
        return map_orm(FeedbackDTO, result)

    def create_seeker_feedback(self, interview_id: int, req: FeedbackCreate):
        interview: Interview = self._session.get(Interview, interview_id)
        result = self._session.execute(
            insert(Feedback).values(**req.dict(), interview_id=interview_id, user_id=interview.seeker.user_id))
        feedback = self._session.get(Feedback, get_primary_key_from_create(result))
        interview.seeker_feedback = feedback
        self._session.merge(interview)
        return feedback.id

    def create_company_feedback(self, interview_id: int, req: FeedbackCreate):
        interview: Interview = self._session.get(Interview, interview_id)
        result = self._session.execute(
            insert(Feedback).values(**req.dict(), interview_id=interview_id, user_id=interview.vacancy.company.user_id))
        feedback = self._session.get(Feedback, get_primary_key_from_create(result))
        interview.company_feedback = feedback
        self._session.merge(interview)
        return feedback.id

    def update_feedback(self, req: FeedbackUpdate):
        feedback: Feedback = self._session.get(Feedback, req.id)
        feedback.rate = req.rate
        feedback.text = req.text
        self._session.merge(feedback)
        return feedback.id

from functools import reduce

from app.models.dto import InterviewDTO
from app.repos.interview_repo import InterviewRepo


class RatingService:
    HIGH_RATE = 5

    def __init__(self, interview_repo: InterviewRepo):
        self._interview_repo = interview_repo

    def get_seeker_rating(self, seeker_id: int):
        interviews: list[InterviewDTO] = self._interview_repo.get_seeker_interview(seeker_id)

        if len(interviews) == 0:
            return self.HIGH_RATE
        rates = [i.company_feedback.rate for i in interviews if i.company_feedback is not None]
        return reduce(lambda a, b: a + b, rates) / len(rates)

    def get_company_rating(self, company_id: int):
        interviews: list[InterviewDTO] = self._interview_repo.get_company_interview(company_id)

        if len(interviews) == 0:
            return self.HIGH_RATE
        rates = [i.seeker_feedback.rate for i in interviews if i.seeker_feedback is not None]
        return reduce(lambda a, b: a + b, rates) / len(rates)

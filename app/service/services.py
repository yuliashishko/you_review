from fastapi import Depends

from app.repos.interview_repo import InterviewRepo
from app.repos.matching_repo import MatchingRepo
from app.repos.repositories import get_seeker_repo, get_matching_repo, get_vacancy_repo, get_interview_repo
from app.repos.seeker_repo import SeekerRepo
from app.repos.vacancy_repo import VacancyRepo
from app.service.matching_serivce import MatchingService
from app.service.rating_service import RatingService


def get_rating_service(interview_repo: InterviewRepo = Depends(get_interview_repo)) -> RatingService:
    return RatingService(interview_repo)


def get_matching_service(seeker_repo: SeekerRepo = Depends(get_seeker_repo),
                         matcher_repo: MatchingRepo = Depends(get_matching_repo),
                         vacancy_repo: VacancyRepo = Depends(get_vacancy_repo),
                         rating_service: RatingService = Depends(get_rating_service)) -> MatchingService:
    return MatchingService(matcher_repo, seeker_repo, vacancy_repo, rating_service)

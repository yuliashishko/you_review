from app.models.dto import SeekerDTO, VacancyDTO
from app.repos.matching_repo import MatchingRepo
from app.repos.seeker_repo import SeekerRepo
from app.repos.vacancy_repo import VacancyRepo
from app.service.rating_service import RatingService


class VacancyForMatching:
    def __init__(self, vac: VacancyDTO):
        self.vac = vac
        self.skills_set = set([s.id for s in vac.skills])
        self.skill_hit = 0


class SeekerForMatching:
    def __init__(self, seeker: SeekerDTO):
        self.seeker = seeker
        self.skills_set = set([s.id for s in seeker.skills])
        self.skill_hit = 0


class MatchingService:
    def __init__(self,
                 matching_repo: MatchingRepo,
                 seeker_repo: SeekerRepo,
                 vacancy_repo: VacancyRepo,
                 rating_service: RatingService):
        self._matching_repo = matching_repo
        self._seeker_repo = seeker_repo
        self._vacancy_repo = vacancy_repo
        self._rating_service = rating_service

    def _populate_vacancy(self, match_set, vacancy: VacancyForMatching, work_format_id: int):
        coef = 1
        if work_format_id == vacancy.vac.work_format.id:
            coef = 3
        rate = self._rating_service.get_company_rating(vacancy.vac.company.id)
        vacancy.skill_hit = rate * coef * len(match_set & vacancy.skills_set) / len(vacancy.skills_set)
        return vacancy

    def _populate_seeker(self, match_set, seeker: SeekerForMatching, work_format_id: int):
        coef = 1
        if work_format_id == seeker.seeker.work_format.id:
            coef = 3
        rate = self._rating_service.get_seeker_rating(seeker.seeker.id)
        seeker.skill_hit = rate * coef * len(match_set & seeker.skills_set) / len(seeker.skills_set)
        return seeker

    def get_vacancies(self, seeker_id: int):
        seeker: SeekerDTO = self._seeker_repo.get_seeker(seeker_id)
        ids = [s.id for s in seeker.skills]
        vacancies: list[VacancyForMatching] = [VacancyForMatching(v) for v in
                                               self._matching_repo.get_vacancies_contains_skills(ids)]
        seeker_skills_set = set([s.id for s in seeker.skills])
        vacancies = list(map(lambda v: self._populate_vacancy(seeker_skills_set, v, seeker.work_format.id), vacancies))
        return list(map(lambda v: v.vac, sorted(vacancies, key=lambda v: v.skill_hit, reverse=True)))[:10]

    def get_seekers(self, vacancy_id: int):
        vacancy: VacancyDTO = self._vacancy_repo.get_vacancy(vacancy_id)
        ids = [s.id for s in vacancy.skills]
        seekers: list[SeekerForMatching] = [SeekerForMatching(s) for s in
                                            self._matching_repo.get_seekers_contains_skills(ids)]
        vacancy_skill_set = set([s.id for s in vacancy.skills])
        seekers = list(map(lambda v: self._populate_seeker(vacancy_skill_set, v, vacancy.work_format.id), seekers))
        return list(map(lambda v: v.seeker, sorted(seekers, key=lambda v: v.skill_hit, reverse=True)))[:10]

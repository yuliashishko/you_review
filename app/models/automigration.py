from app.models.orm import *

# ==========================
from app.models import Base


def get_metadata_for_alembic():
    return Base.metadata

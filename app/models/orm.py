from typing import Optional

from sqlalchemy.orm import relationship, backref

import sqlalchemy as sa

from app.models import Base
from app.models.utils import UniqueMixin


class User(Base):
    __tablename__ = 'user'
    id: Optional[int] = sa.Column(sa.Integer, primary_key=True)
    name: str = sa.Column(sa.String)
    login: str = sa.Column(sa.String)
    password: str = sa.Column(sa.String)


class Company(Base):
    __tablename__ = 'company'
    id: int = sa.Column(sa.Integer, primary_key=True)
    user_id: int = sa.Column(sa.Integer, sa.ForeignKey('user.id', ondelete='CASCADE'), nullable=False)
    user: User = relationship("User", cascade="all, delete", uselist=False)
    vacancies: list['Vacancy'] = relationship('Vacancy', back_populates='company', lazy="joined")


seeker_skill = sa.Table(
    "seeker_skill",
    Base.metadata,
    sa.Column('seeker_id', sa.Integer, sa.ForeignKey('seeker.id'), primary_key=True),
    sa.Column('skill_id', sa.Integer, sa.ForeignKey('skill.id'), primary_key=True)
)


class Seeker(Base):
    __tablename__ = "seeker"
    id: int = sa.Column(sa.Integer, primary_key=True)
    user_id: int = sa.Column(sa.Integer, sa.ForeignKey('user.id', ondelete='CASCADE'), nullable=False)
    user: User = relationship("User", cascade="save-update")
    work_format_id: int = sa.Column(sa.Integer, sa.ForeignKey('work_format.id', ondelete='NO ACTION'))
    work_format = relationship('WorkFormat')
    skills = relationship('Skill', secondary=seeker_skill)

    interviews: list['Interview'] = relationship('Interview', back_populates='seeker', lazy='joined')


class Skill(UniqueMixin, Base):
    __tablename__ = 'skill'

    id: Optional[int] = sa.Column(sa.Integer, primary_key=True)
    name: str = sa.Column(sa.String)

    @classmethod
    def unique_hash(cls, id):
        return id

    @classmethod
    def unique_filter(cls, query, id):
        return query.filter(Skill.id == id)


vacancy_skill = sa.Table(
    "vacancy_skill",
    Base.metadata,
    sa.Column('vacancy_id', sa.Integer, sa.ForeignKey('vacancy.id'), primary_key=True),
    sa.Column('skill_id', sa.Integer, sa.ForeignKey('skill.id'), primary_key=True),
)


class Vacancy(Base):
    __tablename__ = "vacancy"
    id: int = sa.Column(sa.Integer, primary_key=True)
    company_id: int = sa.Column(sa.Integer, sa.ForeignKey('company.id', ondelete='CASCADE'))
    company: Company = relationship("Company", cascade="all, delete", back_populates='vacancies')
    skills = relationship('Skill', secondary=vacancy_skill)
    info: str = sa.Column(sa.String)
    title: str = sa.Column(sa.String)
    work_format_id: int = sa.Column(sa.Integer, sa.ForeignKey('work_format.id', ondelete='NO ACTION'))
    work_format = relationship("WorkFormat")

    interviews: list['Interview'] = relationship("Interview", back_populates="vacancy", lazy='joined')


class WorkFormat(Base):
    __tablename__ = 'work_format'
    id: int = sa.Column(sa.Integer, primary_key=True)
    name: str = sa.Column(sa.String)


class Feedback(Base):
    __tablename__ = 'feedback'
    id: int = sa.Column(sa.Integer, primary_key=True)
    user_id: int = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    user: User = relationship("User", cascade='all, delete')
    rate: int = sa.Column(sa.Integer, sa.CheckConstraint('rate > 0 AND rate <= 5', name='rate_range'), nullable=False)
    text: str = sa.Column(sa.String)
    
    interview_id: int = sa.Column(sa.Integer, sa.ForeignKey('interview.id'), nullable=False)


class InterviewStatus(Base):
    __tablename__ = 'interview_status'
    id: int = sa.Column(sa.Integer, primary_key=True)
    name: str = sa.Column(sa.String, nullable=False)


class Interview(Base):
    __tablename__ = 'interview'
    id: int = sa.Column(sa.Integer, primary_key=True)
    status_id = sa.Column(sa.Integer, sa.ForeignKey("interview_status.id"))
    status = relationship("InterviewStatus")

    seeker_id: int = sa.Column(sa.Integer, sa.ForeignKey('seeker.id', ondelete='CASCADE'))
    seeker = relationship("Seeker", back_populates='interviews')

    vacancy_id: int = sa.Column(sa.Integer, sa.ForeignKey('vacancy.id', ondelete='CASCADE'))
    vacancy = relationship("Vacancy", uselist=False, back_populates='interviews')

    seeker_feedback_id: Optional[int] = sa.Column(sa.Integer, sa.ForeignKey('feedback.id'))
    seeker_feedback: Optional[Feedback] = relationship("Feedback", uselist=False, foreign_keys=[seeker_feedback_id])

    company_feedback_id: Optional[int] = sa.Column(sa.Integer, sa.ForeignKey('feedback.id'))
    company_feedback: Optional[Feedback] = relationship("Feedback", uselist=False, foreign_keys=[company_feedback_id])

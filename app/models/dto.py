from typing import Optional

from pydantic import validator
from pydantic.fields import DeferredType

from app.models import BaseDTO
from app.models.orm import Vacancy, Skill, User


class UserDTO(BaseDTO):
    id: int
    name: str
    login: str
    password: str

    class Meta:
        orm_model = User


class UserCreateDTO(BaseDTO):
    name: str
    login: str
    password: str

    class Meta:
        orm_model = User


class UserUpdate(BaseDTO):
    id: int
    name: str
    login: str
    password: str

    class Meta:
        orm_model = User


# DeferredType for preventing infinite recursion
class CompanyDTO(DeferredType, BaseDTO):
    id: int
    user: UserDTO
    vacancies: list['VacancyDTO'] = []


class CompanyUpdate(BaseDTO):
    id: int
    user: UserUpdate


class VacancyCompanyDTO(BaseDTO):
    id: int
    user: UserDTO


class CompanyCreate(BaseDTO):
    user: UserCreateDTO


class SkillDTO(BaseDTO):
    id: int
    name: str

    class Meta:
        orm_model = Skill


class SkillUpdate(BaseDTO):
    id: int
    name: str

    class Meta:
        orm_model = Skill


class CompanyLinkDTO(BaseDTO):
    id: int


class SeekerDTO(BaseDTO):
    id: int
    user: UserDTO
    skills: Optional[list[SkillDTO]]
    work_format: Optional['WorkFormatDTO']


class SeekerCreate(BaseDTO):
    user: UserCreateDTO
    work_format_id: Optional[int]
    skills: list[int] = []


class SeekerUpdate(BaseDTO):
    id: int
    user: UserUpdate
    work_format_id: Optional['int']
    skills: list[int] = []


class SkillCreate(BaseDTO):
    name: str

    class Meta:
        orm_model = Skill


class WorkFormatDTO(BaseDTO):
    id: int
    name: str


class WorkFormatUpdate(BaseDTO):
    id: int
    name: str


class VacancyDTO(DeferredType, BaseDTO):
    id: int
    company: VacancyCompanyDTO
    skills: Optional[list[SkillDTO]]
    info: str
    title: str
    work_format: WorkFormatDTO

    class Meta:
        orm_model = Vacancy


class SkillShortDTO(BaseDTO):
    id: int

    class Meta:
        orm_model = Skill


class VacancyUpdate(BaseDTO):
    id: int
    skills: list[int] = []
    info: str
    title: str
    work_format_id: int


class Meta:
    orm_model = Vacancy


class VacancyCreate(BaseDTO):
    company_id: int
    skills: list[int] = []
    title: str
    info: str
    work_format_id: int

    class Meta:
        orm_model = Vacancy


class WorkFormatCreate(BaseDTO):
    name: str


class FeedbackDTO(BaseDTO):
    id: int
    user: UserDTO
    rate: int
    text: str


class FeedbackUpdate(BaseDTO):
    id: int
    rate: int
    text: str


class FeedbackCreate(BaseDTO):
    rate: int
    text: str = None

    @validator('rate')
    def rate_must_be_in_range(cls, value):
        if value < 1 or value > 5:
            raise ValueError('Rate should be in range from 1 to 5')
        return value


class InterviewStatusDTO(BaseDTO):
    id: int
    name: str


class InterviewStatusCreate(BaseDTO):
    name: str


class InterviewDTO(BaseDTO):
    id: int
    status: InterviewStatusDTO
    seeker: SeekerDTO
    vacancy: VacancyDTO
    seeker_feedback: Optional[FeedbackDTO]
    company_feedback: Optional[FeedbackDTO]


class InterviewCreate(BaseDTO):
    status_id: int
    seeker_id: int
    vacancy_id: int
    status_id: int


class InterviewUpdate(BaseDTO):
    id: int
    status_id: int


# Forward refs

CompanyDTO.update_forward_refs()
SeekerDTO.update_forward_refs()
SeekerUpdate.update_forward_refs()

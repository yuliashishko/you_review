import uvicorn
from fastapi import FastAPI
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.orm import Session

from app.db import engine
from routers import routers


def startup(app: FastAPI):
    def _startup():
        session_factory = scoped_session(
            sessionmaker(
                engine,
                expire_on_commit=False,
                class_=Session
            )
        )
        app.state.db_engine = engine
        app.state.session_factory = session_factory

    return _startup


def get_app() -> FastAPI:
    app = FastAPI()
    app.on_event("startup")(startup(app))

    app.include_router(router=routers.api_router)
    # app.include_router(skills.router)
    # app.include_router(companies.router)

    return app


if __name__ == "__main__":
    config = uvicorn.Config("dev:get_app", host="0.0.0.0", reload=True, workers=1, port=5000)
    server = uvicorn.Server(config)
    server.run()

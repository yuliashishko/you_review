# FastAPI + SQLAlchemy + Alembic

### Run database only

```shell
$ docker-compose up -d db
```

### Run the project via PyCharm

1. Install dependencies from `requirements.txt`
2. Start database via Docker Compose
3. Create usual python run configuration with `dev.py` as start script
4. Add to this configuration emv variable `DATABASE_URL=postgresql+psycopg2://postgres:postgres@localhost:5432/foo`
5. Before launch check database migration. You can apply the latest DB version via running  `do_upgrade.sh` script.
6. Run python configuration

### Do new migration

1. Run under `project` folder
2. Exec `new_migration.sh` script
3. Setup migration
4. To apply migration, exec `do_upgrade.sh`

### Run full server in docker

```sh
$ docker-compose up -d --build
$ docker-compose exec web alembic upgrade head
```



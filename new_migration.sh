export DATABASE_URL="postgresql+psycopg2://postgres:postgres@localhost:5432/foo"
read -p "Enter migration name: " name

read -p "Create migration '$name'? (Y/N): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1
alembic revision --autogenerate -m $name
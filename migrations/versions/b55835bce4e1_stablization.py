"""stablization

Revision ID: b55835bce4e1
Revises: 701317a3b7c7
Create Date: 2023-01-08 15:45:48.216434

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'b55835bce4e1'
down_revision = '701317a3b7c7'
branch_labels = None
depends_on = None


def drop_old_indexes():
    op.drop_index(op.f('ix_seeker_skill_id'), table_name='seeker_skill')
    op.drop_index(op.f('ix_user_id'), table_name='user')
    op.drop_index(op.f('ix_seeker_id'), table_name='seeker')
    op.drop_index(op.f('ix_seeker_user_id'), table_name='seeker')
    op.drop_index(op.f('ix_company_id'), table_name='company')
    op.drop_index(op.f('ix_company_user_id'), table_name='company')
    op.drop_index(op.f('ix_skill_id'), table_name='skill')


def create_old_indexes():
    op.create_index(op.f('ix_seeker_skill_id'), 'seeker_skill', ['seeker_id', 'skill_id'], unique=True)
    op.create_index(op.f('ix_user_id'), 'user', ['id'], unique=False)
    op.create_index(op.f('ix_seeker_id'), 'seeker', ['id'], unique=False)
    op.create_index(op.f('ix_seeker_user_id'), 'seeker', ['user_id'], unique=False)
    op.create_index(op.f('ix_company_id'), 'company', ['id'], unique=False)
    op.create_index(op.f('ix_company_user_id'), 'company', ['user_id'], unique=False)
    op.create_index(op.f('ix_skill_id'), 'skill', ['id'], unique=False)


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_foreign_key(op.f('fk_seeker_skill_seeker_id_seeker'), 'seeker_skill', 'seeker', ['seeker_id'], ['id'])
    op.create_foreign_key(op.f('fk_seeker_skill_skill_id_skill'), 'seeker_skill', 'skill', ['skill_id'], ['id'])
    op.alter_column('skill', 'name',
                    existing_type=sa.VARCHAR(),
                    nullable=True)
    op.alter_column('user', 'name',
                    existing_type=sa.VARCHAR(),
                    nullable=True)
    op.alter_column('user', 'login',
                    existing_type=sa.VARCHAR(),
                    nullable=True)
    op.alter_column('user', 'password',
                    existing_type=sa.VARCHAR(),
                    nullable=True)
    drop_old_indexes()
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('user', 'password',
                    existing_type=sa.VARCHAR(),
                    nullable=False)
    op.alter_column('user', 'login',
                    existing_type=sa.VARCHAR(),
                    nullable=False)
    op.alter_column('user', 'name',
                    existing_type=sa.VARCHAR(),
                    nullable=False)
    op.alter_column('skill', 'name',
                    existing_type=sa.VARCHAR(),
                    nullable=False)
    op.drop_constraint(op.f('fk_seeker_skill_skill_id_skill'), 'seeker_skill', type_='foreignkey')
    op.drop_constraint(op.f('fk_seeker_skill_seeker_id_seeker'), 'seeker_skill', type_='foreignkey')
    op.create_index('ix_seeker_skill_id', 'seeker_skill', ['seeker_id', 'skill_id'], unique=False)
    op.drop_constraint(op.f('fk_seeker_user_id_user'), 'seeker', type_='foreignkey')
    op.create_foreign_key('seeker_user_id_fkey', 'seeker', 'user', ['user_id'], ['id'])
    op.drop_constraint(op.f('fk_company_user_id_user'), 'company', type_='foreignkey')
    op.create_foreign_key('company_user_id_fkey', 'company', 'user', ['user_id'], ['id'])

    create_old_indexes()
    # ### end Alembic commands ###

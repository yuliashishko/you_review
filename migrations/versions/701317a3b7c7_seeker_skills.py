"""seeker_skills

Revision ID: 701317a3b7c7
Revises: 0851049d0940
Create Date: 2023-01-07 20:50:43.977453

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '701317a3b7c7'
down_revision = '0851049d0940'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('seeker_skill',
                    sa.Column('seeker_id', sa.Integer(), nullable=False),
                    sa.Column('skill_id', sa.Integer(), nullable=False),

                    sa.PrimaryKeyConstraint('seeker_id', 'skill_id'))
    op.create_index(op.f('ix_seeker_skill_id'), 'seeker_skill', ['seeker_id', 'skill_id'], unique=True)


def downgrade():
    op.drop_index(op.f('ix_seeker_skill_id'), 'seeker_skill')
    op.drop_table('seeker_skill')

"""add_skills_companies

Revision ID: 0851049d0940
Revises: bda1327df26e
Create Date: 2023-01-07 19:22:41.984806

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '0851049d0940'
down_revision = 'bda1327df26e'
branch_labels = None
depends_on = None


def create_company():
    op.create_table('company',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('user_id', sa.Integer(), nullable=False),

                    sa.PrimaryKeyConstraint('id'),
                    sa.ForeignKeyConstraint(['user_id'], ['user.id'])
                    )
    op.create_index(op.f('ix_company_id'), 'company', ['id'], unique=False)
    op.create_index(op.f('ix_company_user_id'), 'company', ['user_id'], unique=False)


def create_skills():
    op.create_table('skill',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(), nullable=False),

                    sa.PrimaryKeyConstraint('id'),
                    )
    op.create_index(op.f('ix_skill_id'), 'skill', ['id'], unique=False)


def drop_company():
    op.drop_index(op.f('ix_company_id'), table_name='company')
    op.drop_index(op.f('ix_company_user_id'), table_name='company')
    op.drop_table('company')


def drop_skills():
    op.drop_index(op.f('ix_skill_id'), table_name='skill')
    op.drop_table('skill')

def upgrade():
    create_company()
    create_skills()


def downgrade():
    drop_company()
    drop_skills()
